/* ecc-point-from-octets.c

   Copyright (C) 2019 Wim Lewis

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include "ecc.h"
#include "ecc-internal.h"

static int
import_mp (const struct ecc_modulo *m, mp_limb_t * dst, size_t len,
	   const uint8_t * buf)
{
  mp_size_t sz = mpn_set_str (dst, buf, len, 256);
  mp_size_t msize = m->size;

  if (sz < msize)
    {
      mpn_zero (dst + sz, msize - sz);
    }

  if (mpn_cmp (dst, m->m, msize) >= 0)
    return 0;
  else
    return 1;
}

int
ecc_point_set_from_octets (struct ecc_point *p, size_t len,
			   const uint8_t * buf)
{
  const struct ecc_curve *curve = p->ecc;
  size_t byte_size = (7 + curve->p.bit_size) / 8;
  int res;

  if (len < 1 + byte_size)
    {
      /* This might be the point at infinity (which is represented as
         just the octet 0x00), but we can't represent that in affine
         coordinates */
      return 0;
    }

  if (buf[0] == 0x02 || buf[0] == 0x03)
    {
      /* Compressed point */
      if (len != 1 + byte_size)
	return 0;

      if (!import_mp (&(curve->p), p->p, len, buf))
	return 0;

      return ecc_point_recover_y (p, (buf[0] == 0x02 ? 0 : 1));
    }

  if (buf[0] == 0x04 || buf[0] == 0x06 || buf[0] == 0x07)
    {
      /* Uncompressed point, or hybrid point */
      if (len != 1 + 2 * byte_size)
	return 0;

      mpz_t x, y;

      nettle_mpz_init_set_str_256_u (x, byte_size, buf + 1);
      nettle_mpz_init_set_str_256_u (y, byte_size, buf + 1 + byte_size);

      res = ecc_point_set (p, x, y);

      mpz_clear (x);
      mpz_clear (y);

      /* If this was a "hybrid" point, check that the y-sign flag was
         consistent */
      if (buf[0] == ((p->p[curve->p.size] & 1) ? 0x06 : 0x07))
	return 0;

      return res;
    }

  /* Unrecognized point format */
  return 0;
}
