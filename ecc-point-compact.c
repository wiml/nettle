/* ecc-point-compact.c

   Copyright (C) 2019 Wim Lewis

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include "ecc.h"
#include "ecc-internal.h"

int
ecc_point_set_compact (struct ecc_point *p, const mpz_t x, int y_sign)
{
  const struct ecc_curve *curve = p->ecc;
  mp_size_t size = curve->p.size;

  if (mpz_sgn (x) < 0 || mpz_limbs_cmp (x, curve->p.m, size) >= 0)
    return 0;

  mpz_limbs_copy (p->p, x, size);

  return ecc_point_recover_y (p, y_sign);
}
