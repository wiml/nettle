/* ecc-point-recover.c

   Copyright (C) 2019 Wim Lewis

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>
#include "ecc.h"
#include "ecc-internal.h"
#include "gmp-glue.h"

#define MAX(a, b) ((a) > (b) ? (a) : (b))

static int
ecc_recover_y_weierstrass_itch (const struct ecc_curve *c)
{
  const struct ecc_modulo *m = &(c->p);
  mp_size_t size = m->size;
  return 3 * (m->size) + MAX (2 * size, m->sqrt_itch);
}

/* Test whether a and b are congruent.
   "a" and "b" may be in the range [0, 2p) (that is, almost modulo-reduced, 
   but may contain one multiple of p).

   The "b" input is clobbered.
*/
static int
congruent (const struct ecc_modulo *p, const mp_limb_t * a, mp_limb_t * b)
{
  mp_size_t size = p->size;
  int cy;

  cy = mpn_sub_n (b, b, a, size);

  if (cy == 0)
    {
      /* b was larger than or equal to a; if they were congruent, b now contains either 0 or p. */
      return (mpn_zero_p (b, size) || (mpn_cmp (b, p->m, size) == 0));
    }
  else
    {
      /* b was smaller than a; if they were congruent, b now contains -p. */
      cy = mpn_add_n (b, b, p->m, size);
      if (!cy)
	return 0;
      if (mpn_zero_p (b, size))
	return 1;
      return 0;
    }
}

static int
ecc_recover_y_weierstrass (const struct ecc_curve *c,
			   mp_limb_t * x_y, int y_sign, mp_limb_t * scratch)
{
  const struct ecc_modulo *m = &(c->p);
  mp_size_t size = m->size;
  mp_limb_t *rp = x_y + size;	/* result buffer: second half of X || Y */
  mp_limb_t cy;

  /* Compute y^2 = x^3 - 3*x + b (mod p) */
  mpn_sqr (scratch, x_y, size);
  m->mod (m, scratch);
  if (mpn_sub_1 (scratch, scratch, size, (mp_limb_t) 3))
    {
      mpn_add (scratch, scratch, size, m->m, size);
    }
  mpn_mul (scratch + size, scratch, size, x_y, size);
  cy = mpn_add (scratch + size, scratch + size, 2 * size, c->b, size);
  assert (cy == 0);
  m->mod (m, scratch + size);

  /* At this point, y^2 is at scratch+size */

  if (c->use_redc)
    {
      /* Convert to Montgomery form (at scratch+0) */
      mpn_zero (scratch, size);
      m->mod (m, scratch);

      /* Square root */
      m->sqrt2 (m, scratch + size, scratch, scratch + 3 * size);

      /* Square the result to verify we started with an actual square */
      /* (this tests that the point we're constructing is actually on the curve) */
      ecc_mod_sqr (m, scratch + 2 * size, scratch + size);
      if (!congruent (m, scratch, scratch + 2 * size))
	return 0;

      /* Convert the result from Montgomery back to conventional form */
      mpn_zero (scratch + 2 * size, size);
      m->reduce (m, scratch + size);
    }
  else
    {
      /* Save a copy */
      mpn_copyi (scratch, scratch + size, size);

      /* Square root */
      m->sqrt2 (m, scratch + size, scratch + size, scratch + 3 * size);

      /* Verify that the computed value is in fact a root of y^2; this tests that the point we're constructing is actually on the curve */
      ecc_mod_sqr (m, scratch + 2 * size, scratch + size);
      if (!congruent (m, scratch, scratch + 2 * size))
	return 0;
    }

  /* The computed Y value is now at scratch+size */

  /* Both reduce and mod can leave an extra `p` in their result */
  if (mpn_cmp (scratch + size, m->m, size) >= 0)
    mpn_sub_n (rp, scratch + size, m->m, size);
  else
    mpn_copyi (rp, scratch + size, size);

  /* Check whether we need to negate it to get the other root */
  if ((rp[0] & 1) ? !y_sign : y_sign)
    {
      /* Zero is its own negation, which means we can't satisfy this request */
      /* (This shouldn't be reachable with any of our NIST curves, since none of them
	 have a solution for Y=0) */
      if (mpn_zero_p (rp, size))
	return 0;

      cy = mpn_sub (rp, m->m, size, rp, size);
      assert (cy == 0);
    }

  return 1;
}

int
ecc_point_recover_y (struct ecc_point *p, int y_sign)
{
  const struct ecc_curve *curve = p->ecc;

  if (curve->p.bit_size == 255)
    {
      /* ed25519 special case. FIXME: Do in some cleaner way? */
      /* We don't implement point decompression for Ed25519 curves because the
         standard representation for that algorithm uses a different
         compression style */
      return 0;
    }
  else
    {
      mp_size_t itch = ecc_recover_y_weierstrass_itch (curve);
      mp_limb_t *scratch = gmp_alloc_limbs (itch);
      int res = ecc_recover_y_weierstrass (curve, p->p, y_sign, scratch);
      gmp_free_limbs (scratch, itch);

      return res;
    }
}
