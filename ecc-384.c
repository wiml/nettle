/* ecc-384.c

   Compile time constant (but machine dependent) tables.

   Copyright (C) 2013, 2014 Niels Möller

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

/* Development of Nettle's ECC support was funded by the .SE Internet Fund. */

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <assert.h>

#include "ecc.h"
#include "ecc-internal.h"

#define USE_REDC 0

#include "ecc-384.h"

#if HAVE_NATIVE_ecc_384_modp
#define ecc_384_modp nettle_ecc_384_modp
void
ecc_384_modp (const struct ecc_modulo *m, mp_limb_t *rp);
#elif GMP_NUMB_BITS == 32

/* Use that 2^{384} = 2^{128} + 2^{96} - 2^{32} + 1, and eliminate 256
   bits at a time.

   We can get carry == 2 in the first iteration, and I think *only* in
   the first iteration. */

/* p is 12 limbs, and B^12 - p = B^4 + B^3 - B + 1. We can eliminate
   almost 8 at a time. Do only 7, to avoid additional carry
   propagation, followed by 5. */
static void
ecc_384_modp (const struct ecc_modulo *p, mp_limb_t *rp)
{
  mp_limb_t cy, bw;

  /* Reduce from 24 to 17 limbs. */
  cy = mpn_add_n (rp + 4, rp + 4, rp + 16, 8);
  cy = sec_add_1 (rp + 12, rp + 12, 3, cy);

  bw = mpn_sub_n (rp + 5, rp + 5, rp + 16, 8);
  bw = sec_sub_1 (rp + 13, rp + 13, 3, bw);

  cy += mpn_add_n (rp + 7, rp + 7, rp + 16, 8);
  cy = sec_add_1 (rp + 15, rp + 15, 1, cy);

  cy += mpn_add_n (rp + 8, rp + 8, rp + 16, 8);
  assert (bw <= cy);
  cy -= bw;

  assert (cy <= 2);  
  rp[16] = cy;

  /* Reduce from 17 to 12 limbs */
  cy = mpn_add_n (rp, rp, rp + 12, 5);
  cy = sec_add_1 (rp + 5, rp + 5, 3, cy);
  
  bw = mpn_sub_n (rp + 1, rp + 1, rp + 12, 5);
  bw = sec_sub_1 (rp + 6, rp + 6, 6, bw);
  
  cy += mpn_add_n (rp + 3, rp + 3, rp + 12, 5);
  cy = sec_add_1 (rp + 8, rp + 8, 1, cy);

  cy += mpn_add_n (rp + 4, rp + 4, rp + 12, 5);
  cy = sec_add_1 (rp + 9, rp + 9, 3, cy);

  assert (cy >= bw);
  cy -= bw;
  assert (cy <= 1);
  cy = cnd_add_n (cy, rp, p->B, ECC_LIMB_SIZE);
  assert (cy == 0);
}
#elif GMP_NUMB_BITS == 64
/* p is 6 limbs, and B^6 - p = B^2 + 2^32 (B - 1) + 1. Eliminate 3
   (almost 4) limbs at a time. */
static void
ecc_384_modp (const struct ecc_modulo *p, mp_limb_t *rp)
{
  mp_limb_t tp[6];
  mp_limb_t cy;

  /* Reduce from 12 to 9 limbs */
  tp[0] = 0; /* FIXME: Could use mpn_sub_nc */
  mpn_copyi (tp + 1, rp + 8, 3);
  tp[4] = rp[11] - mpn_sub_n (tp, tp, rp + 8, 4);
  tp[5] = mpn_lshift (tp, tp, 5, 32);

  cy = mpn_add_n (rp + 2, rp + 2, rp + 8, 4);
  cy = sec_add_1 (rp + 6, rp + 6, 2, cy);

  cy += mpn_add_n (rp + 2, rp + 2, tp, 6);
  cy += mpn_add_n (rp + 4, rp + 4, rp + 8, 4);

  assert (cy <= 2);
  rp[8] = cy;

  /* Reduce from 9 to 6 limbs */
  tp[0] = 0;
  mpn_copyi (tp + 1, rp + 6, 2);
  tp[3] = rp[8] - mpn_sub_n (tp, tp, rp + 6, 3);
  tp[4] = mpn_lshift (tp, tp, 4, 32);

  cy = mpn_add_n (rp, rp, rp + 6, 3);
  cy = sec_add_1 (rp + 3, rp + 3, 2, cy);
  cy += mpn_add_n (rp, rp, tp, 5);
  cy += mpn_add_n (rp + 2, rp + 2, rp + 6, 3);

  cy = sec_add_1 (rp + 5, rp + 5, 1, cy);
  assert (cy <= 1);

  cy = cnd_add_n (cy, rp, p->B, ECC_LIMB_SIZE);
  assert (cy == 0);  
}
#else
#define ecc_384_modp ecc_mod
#endif

static void
ecc_384_sqrt (const struct ecc_modulo *m,
	      mp_limb_t *rp,
	      const mp_limb_t *cp,
	      mp_limb_t *scratch)
{
  mp_size_t size = ECC_LIMB_SIZE;

  /* This computes the square root modulo p256 using the identity:

     sqrt(c) = c^(2^382 − 2^126 - 2^94 + 2^30)  (mod P-384)

     which can be seen as a special case of Tonelli-Shanks with e=1.

     The specific sqr/mul schedule is from Routine 3.2.12 of
     "Mathematical routines for the NIST prime elliptic curves", April
     5, 2010, author unknown.
  */

  /* We use our scratch space for several temporaries, all of
     which are 2*size long to allow for multiplication/squaring */
  
#define T1 scratch
#define T2 (scratch + 2*size)
#define T3 (scratch + 4*size)
#define T4 (scratch + 6*size)
#define Tr T2  /* T2 and R are not live at the same time */
#define Tx (scratch + 8*size)

  ecc_mod_pow2n_mul(m, T1, cp, cp, T2, 1);   /* [ 1] T1 <- c^(2^1 - 1) */
  ecc_mod_pow2n_mul(m, Tx, T1, T1, T3, 2);   /* [ 2] T2 <- c^(2^4 - 1) */
  ecc_mod_pow2n_mul(m, T2, Tx, cp, T3, 1);   /* [ 3] T2 <- c^(2^5 - 1) */
  ecc_mod_pow2n_mul(m, T3, T2, T2, Tx, 5);   /* [ 4] T3 <- c^(2^10 - 1) */
  ecc_mod_pow2n_mul(m, T4, T3, T2, Tx, 5);   /* [ 5] T4 <- c^(2^15 - 1) */
  ecc_mod_pow2n_mul(m, T2, T4, T4, Tx, 15);  /* [ 6] T2 <- c^(2^30 - 1) */
  ecc_mod_pow2n    (m, T3, T2,     Tx, 2);   /* [ 7] T3 <- c^(2^32 - 4) */
  ecc_mod_inplc_mul(m, T1, T3,     Tx);      /* [ 8] T1 <- c^(2^32 - 1) */
  ecc_mod_pow2n    (m, T3, T3,     Tx, 28);  /* [ 9] T3 <- c^(2^60 - 2^30) */
  ecc_mod_inplc_mul(m, T2, T3,     Tx);      /*      T2 <- c^(2^60 - 1) */
  ecc_mod_pow2n_mul(m, T3, T2, T2, Tx, 60);  /* [10] T3 <- c^(2^120 - 1) */
  ecc_mod_pow2n_mul(m, Tr, T3, T3, Tx, 120); /* [11] r  <- c^(2^240 - 1) */
  ecc_mod_pow2n_mul(m, T3, Tr, T4, Tx, 15);  /* [12] r  <- c^(2^255 - 1) */
  ecc_mod_pow2n_mul(m, Tr, T3, T1, Tx, 33);  /* [13] r  <- c^(2^288 - 2^32 - 1) */
  ecc_mod_pow2n_mul(m, T3, Tr, cp, T1, 64);  /* [14] r  <- c^(2^352 - 2^96 - 2^64 + 1) */
  ecc_mod_pow2n    (m, rp, T3,     T1, 30);  /* [15] r  <- c^(2^382 - 2^126 - 2^94 + 2^30) */

#undef T1
#undef T2
#undef T3
#undef T4
#undef Tr
#undef Tx
}


const struct ecc_curve _nettle_secp_384r1 =
{
  {
    384,
    ECC_LIMB_SIZE,    
    ECC_BMODP_SIZE,
    ECC_REDC_SIZE,
    ECC_MOD_INV_ITCH (ECC_LIMB_SIZE),
    10 * ECC_LIMB_SIZE,

    ecc_p,
    ecc_Bmodp,
    ecc_Bmodp_shifted,
    ecc_redc_ppm1,
    ecc_pp1h,

    ecc_384_modp,
    ecc_384_modp,
    ecc_mod_inv,
    NULL,
    ecc_384_sqrt,
  },
  {
    384,
    ECC_LIMB_SIZE,    
    ECC_BMODQ_SIZE,
    0,
    ECC_MOD_INV_ITCH (ECC_LIMB_SIZE),
    0,

    ecc_q,
    ecc_Bmodq,
    ecc_Bmodq_shifted,
    NULL,
    ecc_qp1h,

    ecc_mod,
    ecc_mod,
    ecc_mod_inv,
    NULL,
    NULL,
  },

  USE_REDC,
  ECC_PIPPENGER_K,
  ECC_PIPPENGER_C,

  ECC_ADD_JJJ_ITCH (ECC_LIMB_SIZE),
  ECC_MUL_A_ITCH (ECC_LIMB_SIZE),
  ECC_MUL_G_ITCH (ECC_LIMB_SIZE),
  ECC_J_TO_A_ITCH (ECC_LIMB_SIZE),

  ecc_add_jjj,
  ecc_mul_a,
  ecc_mul_g,
  ecc_j_to_a,

  ecc_b,
  ecc_g,
  NULL,
  ecc_unit,
  ecc_table
};

const struct ecc_curve *nettle_get_secp_384r1(void)
{
  return &_nettle_secp_384r1;
}
