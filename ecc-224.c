/* ecc-224.c

   Compile time constant (but machine dependent) tables.

   Copyright (C) 2013, 2014 Niels Möller

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

/* Development of Nettle's ECC support was funded by the .SE Internet Fund. */

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include "ecc.h"
#include "ecc-internal.h"

#if HAVE_NATIVE_ecc_224_modp

#define USE_REDC 0
#define ecc_224_modp nettle_ecc_224_modp
void
ecc_224_modp (const struct ecc_modulo *m, mp_limb_t *rp);

#else
#define USE_REDC (ECC_REDC_SIZE != 0)
#define ecc_224_modp ecc_mod
#endif

#include "ecc-224.h"

#if ECC_REDC_SIZE < 0
# define ecc_224_redc ecc_pm1_redc
#elif ECC_REDC_SIZE == 0
# define ecc_224_redc NULL
#else
# error Configuration error
#endif

static void
ecc_224_sqrt (const struct ecc_modulo *m,
	      mp_limb_t * rp,
	      const mp_limb_t * cp,
	      mp_limb_t * scratch)
{
  mp_size_t size = m->size;
  unsigned int i;

#define S1 (scratch)
#define S1a (scratch + size)
#define S2 (scratch + 2*size)
#define S3 (scratch + 4*size)

  /* Setup for the Tonelli-Shanks algorithm */
  /* scratch is assumed to have space for XXX */

  /* The algorithm starts by decomposing p (the modulus) into e and s  s.t.  (p-1) = s*2^e */
  /* These are precomputed for us as e = ECC_SQRT_E = 96, and s = 2^128 - 1 */
  /* We also have a precomputed ecc_sqrt_z = z^s where z is an arbitrary quadratic nonresidue in p */

  /* compute S1 = c^(2^64 - 1), S1a = c^(2^32 - 1) */
  const mp_limb_t *f = cp;
  for (i = 0; i < 6; i++)
    {
      if (f == S1)
	{
	  mpn_copyi (S3, S1, size);
	  f = S3;
	}
      ecc_mod_pow2n_mul (m, S1, f, f, S2, 1 << i);
      f = S1;
    }

  /* compute S2 = c^(2^127 - 1) */
  mpn_copyi (S1a, S3, size);
  ecc_mod_pow2n_mul (m, S2, S1, S1a, S3, 32); /* S2 = c^(2^96 - 1) */
  for (i = 0; i < 31; i++)
    {
      ecc_mod_sqr (m, S3, S2);
      ecc_mod_mul (m, S2, S3, cp);
    }

  /* Finish up with:

     R <-- c^( (s+1) / 2 ) = c^(2^127)
     c <-- z^s (precomputed)
     t <-- c^s = c^(2^128 - 1)
   */

  ecc_mod_mul (m, S1, S2, cp);
  ecc_mod_sqr (m, S3, S2);
  ecc_mod_mul (m, S1a, S3, cp);

  mpn_copyi (rp, S1, size);
  mpn_copyi (S1, ecc_sqrt_z, size);

  /* We are now set up for the Tonelli-Shanks inner loop:
     R is in *rp
     c is at S1
     t is at S1 + size */
  if (!ecc_tonelli_shanks (m, rp, ECC_SQRT_E, ecc_unit, S1))
    return; /* Our caller will notice that we didn't find an actual sqrt */

#undef S1
#undef S2
#undef S3
}

const struct ecc_curve _nettle_secp_224r1 =
{
  {
    224,
    ECC_LIMB_SIZE,    
    ECC_BMODP_SIZE,
    -ECC_REDC_SIZE,
    ECC_MOD_INV_ITCH (ECC_LIMB_SIZE),
    6 * ECC_LIMB_SIZE,

    ecc_p,
    ecc_Bmodp,
    ecc_Bmodp_shifted,
    ecc_redc_ppm1,
    ecc_pp1h,

    ecc_224_modp,
    USE_REDC ? ecc_224_redc : ecc_224_modp,
    ecc_mod_inv,
    NULL,
    ecc_224_sqrt,
  },
  {
    224,
    ECC_LIMB_SIZE,    
    ECC_BMODQ_SIZE,
    0,
    ECC_MOD_INV_ITCH (ECC_LIMB_SIZE),
    0,

    ecc_q,
    ecc_Bmodq,
    ecc_Bmodq_shifted,
    NULL,
    ecc_qp1h,

    ecc_mod,
    ecc_mod,
    ecc_mod_inv,
    NULL,
    NULL,
  },
  
  USE_REDC,
  ECC_PIPPENGER_K,
  ECC_PIPPENGER_C,

  ECC_ADD_JJJ_ITCH (ECC_LIMB_SIZE),
  ECC_MUL_A_ITCH (ECC_LIMB_SIZE),
  ECC_MUL_G_ITCH (ECC_LIMB_SIZE),
  ECC_J_TO_A_ITCH (ECC_LIMB_SIZE),

  ecc_add_jjj,
  ecc_mul_a,
  ecc_mul_g,
  ecc_j_to_a,

  ecc_b,
  ecc_g,
  NULL,
  ecc_unit,
  ecc_table
};

const struct ecc_curve *nettle_get_secp_224r1(void)
{
  return &_nettle_secp_224r1;
}
