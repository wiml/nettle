/* ecc-point-compact-test.c

   Copyright (C) 2019 Wim Lewis

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#include "testutils.h"

#include "ecc.h"
#include "ecc-internal.h"

static void
dump_testcase_info (const struct ecc_curve *ecc, mpz_t x, int y_sign)
{
  fprintf (stderr, "Curve   = %d bits\nx       = ", ecc->p.bit_size);
  mpz_out_str (stderr, 16, x);
  fprintf (stderr, "\ny-sign  = ");
  if (y_sign)
    {
      fprintf (stderr, "negative (odd)\n");
    }
  else
    {
      fprintf (stderr, "positive (even)\n");
    }
}

static void
dump_result_point (const struct ecc_point *pt)
{
  fprintf (stderr, "Computed values:\nx     = ");
  mpz_t x, y;
  mpz_init (x);
  mpz_init (y);
  ecc_point_get (pt, x, y);
  mpz_out_str (stderr, 16, x);
  fprintf (stderr, "\ny     = ");
  mpz_out_str (stderr, 16, y);
  fprintf (stderr, "\n");
  mpz_clear (x);
  mpz_clear (y);
}

static void
test_point_reconstruction (const struct ecc_curve *ecc,
			   /* X-value */
			   const char *xv,
			   /* Y sign bit */
			   int y_sign,
			   /* Expected Y-value, or NULL to expect failure */
			   const char *yv)
{
  mpz_t x, y_in, x_found, y_found;
  struct ecc_point pt;
  int res;

  mpz_init_set_str (x, xv, 16);
  if (yv)
    {
      mpz_init_set_str (y_in, yv, 16);
    }
  else
    {
      mpz_init (y_in);
    }
  mpz_init (x_found);
  mpz_init (y_found);

  ecc_point_init (&pt, ecc);

  res = ecc_point_set_compact (&pt, x, y_sign);
  if (res > 0)
    {
      if (yv == NULL)
	{
	  fprintf (stderr,
		   "ecc_point_set_compact() succeeded when it should have failed\n");
	  dump_testcase_info (ecc, x, y_sign);
	  dump_result_point (&pt);
	  abort ();
	}

      ecc_point_get (&pt, x_found, y_found);
      if (mpz_cmp (y_in, y_found) != 0)
	{
	  fprintf (stderr, "ecc_point_set_compact() produced incorrect Y\n");
	  dump_testcase_info (ecc, x, y_sign);
	  fprintf (stderr, "y_in  = ");
	  mpz_out_str (stderr, 16, y_in);
	  fprintf (stderr, "\n");
	  dump_result_point (&pt);
	  abort ();
	}

      if (mpz_cmp (x, x_found) != 0)
	{
	  fprintf (stderr, "ecc_point_set_compact() produced incorrect X\n");
	  dump_testcase_info (ecc, x, y_sign);
	  dump_result_point (&pt);
	  abort ();
	}
    }
  else
    {
      if (yv != NULL)
	{
	  fprintf (stderr,
		   "ecc_point_set_compact() failed when it should have succeeded\n");
	  dump_testcase_info (ecc, x, y_sign);
	  abort ();
	}
    }

  ecc_point_clear (&pt);
  mpz_clear (x_found);
  mpz_clear (y_found);
  mpz_clear (x);
  mpz_clear (y_in);
}

static void
test_secp192r1_compact (void)
{
  const struct ecc_curve *ecc = &_nettle_secp_192r1;

  /* The generator point */
  test_point_reconstruction (ecc,
			     "188da80eb03090f67cbf20eb43a18800f4ff0afd82ff1012",
			     1,
			     "07192b95ffc8da78631011ed6b24cdd573f977a11e794811");

  /* Misc points */
  test_point_reconstruction (ecc,
			     "1faee4205a4f669d2d0a8f25e3bcec9a62a6952965bf6d31",
			     0,
			     "5ff2cdfa508a2581892367087c696f179e7a4d7e8260fb06");

  test_point_reconstruction (ecc,
			     "3afc9fe857c9cac94b333b0e0c2f5eaca8352670472e6c9a",
			     1,
			     "fffffffffffffffeffffffffffffffffffffffffffffffff");

  test_point_reconstruction (ecc,
			     "6640909960af1640197342c0c58b40406b56a622c1258431",
			     0,
			     "000000000000000000000001000000000000000000000000");
}

static void
test_secp224r1_compact (void)
{
  const struct ecc_curve *ecc = &_nettle_secp_224r1;

  /* The generator point */
  test_point_reconstruction (ecc,
			     "b70e0cbd6bb4bf7f321390b94a03c1d356c21122343280d6115c1d21",
			     0,
			     "bd376388b5f723fb4c22dfe6cd4375a05a07476444d5819985007e34");


  /* Special cases, eg to increase branch coverage in our
     implementation (thanks to SageMath / Pari for computations) */
  test_point_reconstruction (ecc,	/* Y = 1 */
			     "3b5889352ddf7468bf8c0729212aa1b2a3fcb1a844b8be91abb753d5",
			     1,
			     "00000000000000000000000000000000000000000000000000000001");
  test_point_reconstruction (ecc,	/* Y = -1 */
			     "3b5889352ddf7468bf8c0729212aa1b2a3fcb1a844b8be91abb753d5",
			     0,
			     "ffffffffffffffffffffffffffffffff000000000000000000000000");
  test_point_reconstruction (ecc,	/* Y^2 intermediate is 2^98-1 */
			     "760737a5a69ba5f84740bcafca96f6cc3049d98fa95a45c9d159126b",
			     1,
			     "86d5a7d677bc09173bd94be84efb59936faca004c9bada763b39a531");

  /* From Google Wycheproof */
  test_point_reconstruction (ecc,	/* testcase 21 - edge cases for ephemeral key */
			     "00000000000000ffffffffffffff0000000000000100000000000000",
			     0,
			     "73ca5f8f104997a2399e0c7f25e72a75ec29fc4542533d3fea89a33a");
  test_point_reconstruction (ecc,	/* testcase 26 - edge cases for ephemeral key */
			     "0a15c112ff784b1445e889f955be7e3ffdf451a2c0e76ab5cb32cf41",
			     0,
			     "3d4df973c563c6decdd435e4f864557e4c273096d9941ca4260a266e");
  test_point_reconstruction (ecc,	/* testcase 28 - edge cases for ephemeral key */
			     "661ac958c0febbc718ccf39cefc6b66c4231fbb9a76f35228a3bf5c3",
			     1,
			     "103b8040e3cb41966fc64a68cacb0c14053f87d27e8ed7bf2d7fe51b");
  test_point_reconstruction (ecc,	/* testcase 65 - invalid public key */
			     "0ca753db5ddeca474241f8d2dafc0844343fd0e37eded2f0192d51b2",
			     0, NULL);

}

static void
test_secp256r1_compact (void)
{
  const struct ecc_curve *ecc = &_nettle_secp_256r1;

  /* The generator point */
  test_point_reconstruction (ecc,
			     "6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296",
			     1,
			     "4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5");

  /* Some randomly generated public key points */
  test_point_reconstruction (ecc,
			     "bd937cced2832294ef2373921b9a93dc6654a42d4acc0f19cd2f8b5ebc4976cb",
			     1,
			     "1566e322666211348e8733781c8815d4edf8fb16b71ac02d244eee5d128605d9");
  test_point_reconstruction (ecc,
			     "2b0c788d3b01a1d16cb45b74301f9be9bb50632279980483ee23ab2ec88abf6e",
			     0,
			     "5eb630230b9ece843f25f2581e6b22137224607f2ec25fae44262844bfe2635a");

  /* Invalid public keys: from Google Wycheproof */
  test_point_reconstruction (ecc,
			     "fd4bf61763b46581fd9174d623516cf3c81edd40e29ffa2777fb6cb0ae3ce535",
			     0, NULL);
  test_point_reconstruction (ecc,
			     "efdde3b32872a9effcf3b94cbf73aa7b39f9683ece9121b9852167f4e3da609b",
			     1, NULL);

  /* Special cases tailored to our implementation (thanks to Wolfram Alpha for computations) */

  /* Incur an annoying underflow in computation of Y */
  test_point_reconstruction (ecc,
			     "507442007322aa895340cba4abc2d730bfd0b16c2c79a46815f8780d2c55a2dd",
			     0,
			     "b9e6295f66bf0aea9c55edc7e4383098742e58d6b6043ee4f49634dd2e5060d2");

  /* X = 0 */
  test_point_reconstruction (ecc,
			     "0000000000000000000000000000000000000000000000000000000000000000",
			     0,
			     "66485c780e2f83d72433bd5d84a06bb6541c2af31dae871728bf856a174f93f4");

  /* Y = 1 */
  test_point_reconstruction (ecc,
			     "09e78d4ef60d05f750f6636209092bc43cbdd6b47e11a9de20a9feb2a50bb96c",
			     1,
			     "0000000000000000000000000000000000000000000000000000000000000001");

  /* Y = sqrt(-1), not on the curve, maximal intermediate value */
  test_point_reconstruction (ecc,
			     "5a7791a91a311985212b9666ee834b704fdc69b0936c0cc43baa5875cf7f75c8",
			     1, NULL);

  /* As above, but large intermediate value in Montgomery representation with B = 2^256 */
  test_point_reconstruction (ecc,
			     "21747cc7b9bb570fa3c1d8a29577ad5912426f033e3f675719577db83a1f01f7",
			     0, NULL);
  test_point_reconstruction (ecc,
			     "4538f734977b56ee3e90841be9dae751f024b109eb7b83025cf2099f0764a3b7",
			     0,
			     "32fb1b2b0f1d1a553e1a328d84ad8f00228d555aad10b3ef7da720642f4b0680");
}

static void
test_secp384r1_compact (void)
{
  const struct ecc_curve *ecc = &_nettle_secp_384r1;

  /* The generator point */
  test_point_reconstruction (ecc,
			     "aa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7",
			     1,
			     "3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f");

  /* Some vectors from Wycheproof */
  test_point_reconstruction (ecc,
			     "6fcaf82d982d222d6096ba83e55b1c7dcb71a41e88f323333f44284d95c4bd3616da7a1bef928f31c26f885ba7adb487",
			     1,
			     "826fde2ed9f5649c11cf8465f8bf8ad50f68914936fc39666f68219d066506bea4001fdc816c9a90e7e2afb19bea085f");
  test_point_reconstruction (ecc,
			     "4424530ea70bace90601f8d5869e4179a6cd689b6a18fdfec50cecf17cb836d24820211ada67815b42c2c2606303f69e",
			     0, NULL);
  test_point_reconstruction (ecc,
			     "0000000000000000000000000000000000000000000000000000000036a2907c00000000000000000000000000000000",
			     0,
			     "ffffffff80a84965feb87c2405b6984d06305987590f4916302be9b7313a4c3a6718deac25c07d2c25d17161710c84ee");
  test_point_reconstruction (ecc,
			     "0000000000000000000000000000000000000000000000000000000036a2907c00000000000000000000000000000000",
			     1,
			     "000000007f57b69a014783dbfa4967b2f9cfa678a6f0b6e9cfd41648cec5b3c498e72152da3f82d3da2e8e9f8ef37b11");
  test_point_reconstruction (ecc,
			     "fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeffffffff0000000000000000fffffffe",
			     1,
			     "732152442fb6ee5c3e6ce1d920c059bc623563814d79042b903ce60f1d4487fccd450a86da03f3e6ed525d02017bfdb3");
  test_point_reconstruction (ecc,
			     "0000000000000000ffffffffffffffff0000000000000000ffffffffffffffff00000000000000010000000000000001",
			     0,
			     "141b9ee5310ea8170131b604484a6d677ed42576045b7143c026710ae92b277afbbea0c4458c220d561e69404dc7d888");
}

static void
test_secp521r1_compact (void)
{
  const struct ecc_curve *ecc = &_nettle_secp_521r1;

  /* The generator point */
  test_point_reconstruction (ecc,
			     "0c6858e06b70404e9cd9e3ecb662395b4429c648139053fb521f828af606b4d3d"
			     "baa14b5e77efe75928fe1dc127a2ffa8de3348b3c1856a429bf97e7e31c2e5bd66",
			     0,
			     "11839296a789a3bc0045c8a5fb42c7d1bd998f54449579b446817afbd17273e66"
			     "2c97ee72995ef42640c550b9013fad0761353c7086a272c24088be94769fd16650");

  /* Special cases tailored to our implementation (thanks to Wolfram Alpha for computations) */
  test_point_reconstruction (ecc,
			     "18bde04c38d7081cf87b4b9526163c3cead63de0cfcdb3acaedeb830fadda25fe"
			     "bcd096cb771c0bc95e3eaaeab69c94e66296108383a913caf87830fc19f410ba3e",
			     0, NULL /* Y = sqrt(-1) */ );
  test_point_reconstruction (ecc,
			     "0d9cb7a32dab342f863edb340f3ea61ddf833e755ce66bb1a918a42714ba05bcd"
			     "f4ff10994f616a9d80cd0b48b326e3a8a2a8f5634d824875b6e71fb7cddd7b5018",
			     1, "1");

  /* Some Wycheproof edge cases */
  test_point_reconstruction (ecc,
			     "00000000000000000000000000000000000000000000000000000000000000000"
			     "000000000000000000000000000000000000000000000000000000000000000001",
			     0,
			     "010e59be93c4f269c0269c79e2afd65d6aeaa9b701eacc194fb3ee03df47849bf"
			     "550ec636ebee0ddd4a16f1cd9406605af38f584567770e3f272d688c832e843564");
  test_point_reconstruction (ecc,
			     "1ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
			     "fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd",
			     0,
			     "010e59be93c4f269c0269c79e2afd65d6aeaa9b701eacc194fb3ee03df47849bf"
			     "550ec636ebee0ddd4a16f1cd9406605af38f584567770e3f272d688c832e843564");
  test_point_reconstruction (ecc,
			     "1ff00000000000000000000000000000000ffffffffffffffffffffffffffffff"
			     "ff0000000000000000000000000000000100000000000000000000000000000000",
			     1,
			     "0b5e1191b449fa1ebdbd677daa48f90e2d1d6c058c877087cafd9364d99dbb283"
			     "c68402e6e6c5f5411b2ed42824d8b280ceb910aba6847883a7e3780e2132af41c1");

  /* Some invalid points */
  test_point_reconstruction (ecc,
			     "047b9cf28e04b38796858545d60d6133fbdc20ede086e5d95111c982b8c276628"
			     "235e536c075637a97c0a6c30d02b83b19e578203473eea16dfdeaeccb1dc0d9b19",
			     1, NULL);
  test_point_reconstruction (ecc,
			     "0429cb431c18f5f4e4e502f74214e6ac5ec2c3f86b830bac24de95feae142ca7d"
			     "9aa8aa5b34f55af4b2848f2e6ba6df4c3ecd401a1d7b2a8287a332b202196fadbb",
			     0, NULL);

}

static void
test_out_of_range (void)
{
  mpz_t x;
  struct ecc_point pt;
  int res;

  mpz_init (x);

  mpz_set_si (x, -1);
  ecc_point_init (&pt, &_nettle_secp_521r1);
  res = ecc_point_set_compact (&pt, x, 0);
  if (res != 0)
    {
      fprintf (stderr, "Failed to reject X<0\n");
      dump_result_point (&pt);
      abort ();
    }
  ecc_point_clear (&pt);

  mpz_set_si (x, 0);
  ecc_point_init (&pt, &_nettle_secp_256r1);
  res = ecc_point_set_compact (&pt, x, 0);
  if (res == 0)
    {
      fprintf (stderr, "Failed to accept X==0 on P-256\n");
      abort ();
    }
  ecc_point_clear (&pt);

  mpz_ui_pow_ui (x, 2, 521);
  mpz_sub_ui (x, x, 1);
  ecc_point_init (&pt, &_nettle_secp_521r1);
  res = ecc_point_set_compact (&pt, x, 0);
  if (res != 0)
    {
      fprintf (stderr, "Failed to reject X==p\n");
      dump_result_point (&pt);
      abort ();
    }
  ecc_point_clear (&pt);

  mpz_clear (x);
}

void
test_main (void)
{
  test_secp192r1_compact ();
  test_secp224r1_compact ();
  test_secp256r1_compact ();
  test_secp384r1_compact ();
  test_secp521r1_compact ();
  test_out_of_range ();
}
