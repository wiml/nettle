/* ecc-point-compact-test.c

   Copyright (C) 2019 Wim Lewis

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#include "testutils.h"

#include "ecc.h"
#include "base16.h"

static void
test_point_from_octets (const struct ecc_curve *ecc,
			/* External representation */
			const char *pt,
			/* Expected X and Y */
			const char *xv, const char *yv)
{
  struct ecc_point p;
  int res;
  struct base16_decode_ctx hexc;
  uint8_t *pbuf;
  size_t pbuf_capacity, pbuf_len;

  base16_decode_init (&hexc);
  pbuf_capacity = BASE16_DECODE_LENGTH (strlen (pt));
  pbuf = malloc (pbuf_capacity + 1);
  pbuf_len = 0;
  if (!base16_decode_update (&hexc, &pbuf_len, pbuf, strlen (pt), pt) ||
      !base16_decode_final (&hexc))
    {
      abort ();
    }

  ecc_point_init (&p, ecc);
  res = ecc_point_set_from_octets (&p, pbuf_len, pbuf);
  free (pbuf);

  if (res)
    {
      mpz_t xexpected, xfound, yexpected, yfound;

      if (!xv)
	{
	  fprintf (stderr,
		   "ecc_point_set_from_octets succeeded when it should have failed.\npt = \"%s\"\n",
		   pt);
	  abort ();
	}

      mpz_init (xfound);
      mpz_init (yfound);
      ecc_point_get (&p, xfound, yfound);

      mpz_init_set_str (xexpected, xv, 16);
      mpz_init_set_str (yexpected, yv, 16);

      if (mpz_cmp (xexpected, xfound) != 0)
	{
	  fprintf (stderr, "X-coordinate mismatch\n");
	  res = 0;
	}
      if (mpz_cmp (yexpected, yfound) != 0)
	{
	  fprintf (stderr, "Y-coordinate mismatch\n");
	  res = 0;
	}

      if (!res)
	abort ();

      mpz_clear (xfound);
      mpz_clear (yfound);
      mpz_clear (xexpected);
      mpz_clear (yexpected);
    }
  else
    {
      if (xv)
	{
	  fprintf (stderr,
		   "ecc_point_set_from_octets failed when it should have succeeded.\npt = \"%s\"\n",
		   pt);
	  abort ();
	}
    }
}

void
test_main (void)
{

  test_point_from_octets (&_nettle_secp_256r1, "", NULL, NULL);

  /* Technically a valid point representation, but it's the point at
     infinity, which we can't represent in affine coordinates and
     which isn't a valid public key. */
  test_point_from_octets (&_nettle_secp_256r1, "00", NULL, NULL);

  /* Strings with invalid lengths */
  test_point_from_octets (&_nettle_secp_256r1, "02", NULL, NULL);
  test_point_from_octets (&_nettle_secp_256r1, "04", NULL, NULL);
  test_point_from_octets (&_nettle_secp_256r1, "06", NULL, NULL);

  test_point_from_octets (&_nettle_secp_224r1,
			  "01AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
			  NULL, NULL);

  /* A valid point */
  test_point_from_octets (&_nettle_secp_256r1,
			  "04"
			  "450b6b6e2097178e9d2850109518d28eb3b6ded2922a5452003bc2e4a4ec775c"
			  "894e90f0df1b0e6cadb03b9de24f6a22d1bd0a4a58cd645c273cae1c619bfd61",
			  "450b6b6e2097178e9d2850109518d28eb3b6ded2922a5452003bc2e4a4ec775c",
			  "894e90f0df1b0e6cadb03b9de24f6a22d1bd0a4a58cd645c273cae1c619bfd61");

  /* A point not on the curve */
  test_point_from_octets (&_nettle_secp_256r1,
			  "04"
			  "450b6b6e2097178e9d2850109518d28eb3b6ded2922a5452003bc2e4a4ec775c"
			  "894e90f0df1b0e6cadb03b9de24f6a22d1cd0a4a58cd645c273cae1c619bfd61",
			  NULL, NULL);
  test_point_from_octets (&_nettle_secp_256r1,
			  "04"
			  "ffffffff00000001000000000000000000000000ffffffffffffffffffffffff"
			  "0000000000000000000000000000000000000000000000000000000000000000",
			  NULL, NULL);
  test_point_from_octets (&_nettle_secp_224r1,  /* X > p */
			  "02"
			  "ffffffffffffffffffffffffffffffffffffffffffffffffffff0000",
			  NULL, NULL);

  /* Compressed points */
  test_point_from_octets (&_nettle_secp_256r1,
			  "0362d5bd3372af75fe85a040715d0f502428e07046868b0bfdfa61d731afe44f26",
			  "62d5bd3372af75fe85a040715d0f502428e07046868b0bfdfa61d731afe44f26",
			  "ac333a93a9e70a81cd5a95b5bf8d13990eb741c8c38872b4a07d275a014e30cf");
  test_point_from_octets (&_nettle_secp_256r1,
			  "02"
			  "58fd4168a87795603e2b04390285bdca6e57de6027fe211dd9d25e2212d29e62",
			  "58fd4168a87795603e2b04390285bdca6e57de6027fe211dd9d25e2212d29e62",
			  "080d36bd224d7405509295eed02a17150e03b314f96da37445b0d1d29377d12c");

  /* An invalid compressed point (wrong length) */
  test_point_from_octets (&_nettle_secp_256r1,
			  "02"
			  "58fd4168a87795603e2b04390285bdca6e57de6027fe211dd9d25e2212d29e",
			  NULL, NULL);
  test_point_from_octets (&_nettle_secp_256r1,
			  "02"
			  "58fd4168a87795603e2b04390285bdca6e57de6027fe211dd9d25e2212d29e625a",
			  NULL, NULL);

  test_point_from_octets (&_nettle_secp_384r1,
			  "02"
			  "58fd4168a87795603e2b04390285bdca6e57de6027fe211dd9d25e2212d29e62",
			  NULL, NULL);

  /* Hybrid points, and internally-inconsistent hybrid points */
  test_point_from_octets (&_nettle_secp_256r1,
			  "06"
			  "58fd4168a87795603e2b04390285bdca6e57de6027fe211dd9d25e2212d29e62"
			  "080d36bd224d7405509295eed02a17150e03b314f96da37445b0d1d29377d12c",
			  "58fd4168a87795603e2b04390285bdca6e57de6027fe211dd9d25e2212d29e62",
			  "080d36bd224d7405509295eed02a17150e03b314f96da37445b0d1d29377d12c");
  test_point_from_octets (&_nettle_secp_256r1,
			  "07"
			  "58fd4168a87795603e2b04390285bdca6e57de6027fe211dd9d25e2212d29e62"
			  "080d36bd224d7405509295eed02a17150e03b314f96da37445b0d1d29377d12c",
			  NULL, NULL);
  test_point_from_octets (&_nettle_secp_256r1,
			  "06"
			  "58fd4168a87795603e2b04390285bdca6e57de6027fe211dd9d25e2212d29e62",
			  NULL, NULL);
  test_point_from_octets (&_nettle_secp_256r1,
			  "07"
			  "e9484e58f3331b66ffed6d90cb1c78065fa28cfba5c7dd4352013d3252ee4277"
			  "bd7503b045a38b4b247b32c59593580f39e6abfa376c3dca20cf7f9cfb659e13",
			  "e9484e58f3331b66ffed6d90cb1c78065fa28cfba5c7dd4352013d3252ee4277",
			  "bd7503b045a38b4b247b32c59593580f39e6abfa376c3dca20cf7f9cfb659e13");

  test_point_from_octets (&_nettle_secp_256r1,
			  "06"
			  "e9484e58f3331b66ffed6d90cb1c78065fa28cfba5c7dd4352013d3252ee4277"
			  "bd7503b045a38b4b247b32c59593580f39e6abfa376c3dca20cf7f9cfb659e13",
			  NULL, NULL);

  test_point_from_octets (&_nettle_secp_256r1,
			  "07"
			  "e9484e58f3331b66ffed6d90cb1c78065fa28cfba5c7dd4352013d3252ee4277"
			  "bd7503b045a38b4b247b32c59593581f39e6abfa376c3dca20cf7f9cfb659e13",
			  NULL, NULL);

}
