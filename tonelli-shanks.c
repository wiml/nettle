/* tonelli-shanks.c

   Copyright (C) 2019 Wim Lewis

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include "ecc-internal.h"

/* Test whether a and b are congruent.
   "a" may be in the range [0, 2p) (that is, almost modulo-reduced,
   but may contain one multiple of p).

   "b" is assumed to be fully reduced mod p.
*/
static int
congruent (const struct ecc_modulo * p, const mp_limb_t * a,
	   const mp_limb_t * b, mp_limb_t * scratch)
{
  mp_size_t size = p->size;
  int cy;

  cy = mpn_sub_n (scratch, a, b, size);

  if (cy == 0)
    {
      /* a was larger than or equal to b; if they were congruent,
         scratch now contains either 0 or p. */
      return (mpn_zero_p (scratch, size) ||
	      0 == mpn_cmp (scratch, p->m, size));
    }
  else
    {
      /* a was smaller than b; they can't be congruent. */
      return 0;
    }
}

/* Inner loop of the Tonelli-Shanks algorithm for square root.

   The input to this part of the algorithm is three bignums (R, c, t) and
   a smallnum (e).  R should be in *rp (which is also where the root will
   be stored); c and t should be at the beginning of scratch;
   and space for another two variables (each 2*size limbs long) should exist in scratch.
*/
int
ecc_tonelli_shanks (const struct ecc_modulo * m,
		    mp_limb_t * rp,
		    unsigned int e,
		    const mp_limb_t * unit,
		    mp_limb_t * scratch)
{
  mp_size_t size = m->size;

#define c  (scratch)
#define t  (scratch +  size)
#define S1 (scratch + 2*size)
#define S2 (scratch + 4*size)
  for (;;)
    {

      /* Search for i such that t^(2^i) = 1; 0 <= i < e */
      unsigned i = 0;
      if (congruent (m, t, unit, S2))
	{
	  /* Return R, which is conveniently stored at *rp already */
	  return 1;
	}

      /* Repeatedly compute: dst_i <-- t^(2^i); i++ */
      for (;;)
	{
	  if ((i + 1) >= e)
	    {
	      return 0;		/* Failure: our input wasn't a quadratic residue */
	    }
	  mp_limb_t *dst_i   = (i % 2 == 0) ? S1 : S2;
	  mp_limb_t *other_i = (i % 2 == 0) ? S2 : S1;
	  ecc_mod_sqr (m, dst_i, (i == 0) ? t : other_i);
	  i++;
	  if (congruent (m, dst_i, unit, other_i))
	    {
	      break;
	    }
	}

      /* i is at least 1, and strictly less than e, which guarantees this loop will terminate */

      /* Update our loop variables:
         e' <-- i
         c' <-- c^(2^(e-i))
         t' <-- t*c'
         R' <-- R*c^(2^(e-i-1))
       */

      ecc_mod_pow2n (m, S1, c, S2, e - i - 1);
      e = i;
      ecc_mod_mul (m, S2, rp, S1);
      mpn_copyi (rp, S2, size);

      ecc_mod_sqr (m, S2, S1);
      mpn_copyi (c, S2, size);

      ecc_mod_mul (m, S2, t, c);
      mpn_copyi (t, S2, size);
    }
#undef c
#undef t
#undef S1
#undef S2
}
